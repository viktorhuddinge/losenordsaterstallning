﻿# Export Förnamn, efternamn, klass, skola, användranamn. Samma användarnamn med löpnummer separat lista.
# 0.2 Ändrat generate-password så inte -, = eller + sätts i början.


#Bygga Guit
Add-Type -AssemblyName PresentationCore, PresentationFramework
Add-Type -AssemblyName System.Windows.Forms

[xml]$XAML = @'

<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:PasswordMulitReset"
        Title="Lösenordshantering elever" Height="448.97" Width="647.598">
    <Grid>
        <Label Name="lableSkola" Content="Skola" HorizontalAlignment="Left" Margin="26,33,0,0" VerticalAlignment="Top"/>
        <Label Name="labelKlass" Content="Klass" HorizontalAlignment="Left" Margin="26,64,0,0" VerticalAlignment="Top" RenderTransformOrigin="0.538,-0.421"/>
        <ComboBox Name="comboboxSkola" HorizontalAlignment="Left" Margin="129,37,0,0" VerticalAlignment="Top" Width="120"/>
        <ComboBox Name="comboboxKlass" HorizontalAlignment="Left" Margin="129,68,0,0" VerticalAlignment="Top" Width="120"/>
        <ListBox Name="listboxAnvändare" HorizontalAlignment="Left" Height="376" Margin="333,10,0,0" VerticalAlignment="Top" Width="297"/>
        <Button Name="buttonsökanvändare" Content="Sök" HorizontalAlignment="Left" Margin="129,95,0,0" VerticalAlignment="Top" Width="120"/>
        <Label Name="labelSökAnvändare" Content="Sök användare" HorizontalAlignment="Left" Margin="26,92,0,0" VerticalAlignment="Top" RenderTransformOrigin="0.538,-0.421"/>
        <Label Name="LabelResetPassword" Content="Sätt nytt lösenord" HorizontalAlignment="Left" Margin="26,123,0,0" VerticalAlignment="Top" RenderTransformOrigin="0.538,-0.421"/>
        <Button Name="buttonSättaLösenord" Content="Sätt lösenord" HorizontalAlignment="Left" Margin="129,126,0,0" VerticalAlignment="Top" Width="120"/>

    </Grid>
</Window>
'@




$reader=(New-Object System.Xml.XmlNodeReader $xaml) 
try{$Form=[Windows.Markup.XamlReader]::Load( $reader )}
catch{Write-Host "Unable to load Windows.Markup.XamlReader. Some possible causes for this problem include: .NET Framework is missing PowerShell must be launched with PowerShell -sta, invalid XAML code was encountered."; exit}
$xaml.SelectNodes("//*[@Name]") | ForEach-Object {Set-Variable -Name ($_.Name) -Value $Form.FindName($_.Name)}

#Imortera AD-modul
Import-Module ActiveDirectory
$listboxAnvändare.SelectionMode = "Extended"

#Funktioner
    # Hämta skolor och fyll combobox
        function get-skolorFyllCombo 
        {
            $skolor = Get-ADOrganizationalUnit -SearchBase "OU=Skola,OU=Test,DC=adm,DC=huddinge,DC=se" -SearchScope OneLevel -Filter * | Select-Object name, DistinguishedName
                foreach ($skola in $skolor)
                {
                    $comboboxSkola.AddChild($skola.name)
                }
                
        }
        get-skolorFyllCombo
    
    # Hämta klass från skola
        function get-klassfyllcombo 
        {
            if ($null -ne $comboboxSkola.SelectedItem)
            {
                $Klasser = Get-ADUser -Filter * -SearchBase ("OU=" + $comboboxSkola.SelectedItem + ",OU=Skola,OU=Test,DC=adm,DC=huddinge,DC=se") -Properties Department | Sort-Object Department -Unique | Select-Object department
            $comboboxKlass.Items.Clear()    
                foreach ($klass in $klasser)
                {
                    
                    $comboboxKlass.AddChild($klass.department)
                }
            }
            
        }
      # Sök användare
        function get-elever 
        {
            $eleverIKlass = Get-ADUser -Filter {Department -eq $comboboxKlass.SelectedItem} -SearchBase ("OU=" + $comboboxSkola.SelectedItem + ",OU=Skola,OU=Test,DC=adm,DC=huddinge,DC=se") | Select-Object name,SamAccountName,GivenName,Surname
                if (-not $listboxAnvändare.Items.IsEmpty)
                {
                    $listboxAnvändare.Items.Clear()
                }
                foreach ($elev in $eleverIKlass)
                {
                    
                    $listboxAnvändare.Items.Add($elev.SamAccountName + "," + $elev.Surname + " " + $elev.GivenName)
                } 
        }     

      # Skapa lösenord
      function Generate-Password 
      {
      $randomAsciiletters = (65..72) + (74..78) + (80..90) + (97..107) + (109..110) + (112..122) | Get-Random -Count 5 | ForEach-Object {[char]$_}
      [string]$randomNumber = 2..9 | Get-Random
      $randomSpecialCharacter = "+", "-", "=" | Get-Random
      $randomIndex1 = $randomAsciiletters + (2..9 | Get-Random) | Get-Random
      $randomIndex2 = $randomAsciiletters + $randomNumber + $randomSpecialCharacter | Sort-Object {Get-Random}
      
      $global:password = [string]$randomIndex1 + (-join $randomIndex2)
      $password
      }
     
     # lista användare med respektive lösenord
        function List-UsersAndPassword
                  {
                    $Global:usersAndPasswords = @()
                    foreach ($användare in $listboxAnvändare.SelectedItems)
                        {
                            $användare2 = ($användare.Split(",")[0])
                            $adAtributes = get-aduser -Identity $användare2  -Properties department, company
                            $password = Generate-Password
                            $Global:usersAndPasswords += [PSCustomObject]@{
                                  "Förnamn" = $adAtributes.GivenName
                                  "Efternamn" =$adAtributes.Surname
                                  "Klass" = $adAtributes.Department
                                  "skola" =$adAtributes.Company
                                  "Användarnamn" = $användare2
                                  "Lösenord" = $password
                                                                          }
                                    Remove-Variable password
                       
                  }
                  
                  }
     # Sätta lösenord
        function set-userpassword 
                    {
                       if ($usersAndPasswords.count -lt 39)
                       {
                        $usersToResetFormated = @()
                        $usersToreset = $usersAndPasswords.Användarnamn | Sort-Object -Descending
                        $usersWithoutAccess = @()
                            foreach ($usertoReset in $usersToreset)
                            {
                                $usersToResetFormated += $usertoReset + "`n"
                            }
                                    #Messagebox
                                    $buttonType = "YesNo" 
                                    $messageIcon = "warning"
                                    $messageBoxTitle = "Återställa lösenord för följande användare?"
                                    $popUpMessage =  [System.Windows.MessageBox]::Show($usersToResetFormated, $messageBoxTitle ,$buttonType, $messageIcon)

                                        switch  ($popUpMessage) {

                                          'Yes' {  
                                                    
                                                    foreach ($userAndPassword in $usersAndPasswords)
                                                    {
                                                        $NewPassword = ConvertTo-SecureString -AsPlainText $userAndPassword.Lösenord -Force
                                                       
                                                       try
                                                       {
                                                           Set-ADAccountPassword -Identity $userAndPassword.Användarnamn -NewPassword $NewPassword -Reset
                                                       }
                                                       catch [System.UnauthorizedAccessException]
                                                       {
                                                           [System.Windows.MessageBox]::Show("Du har inga rättigheter för att sätta lösenord för $($userAndPassword.Användarnamn)","Åtkomst nekad","Ok","Warning")
                                                           $usersWithoutAccess += $userAndPassword.Användarnamn

                                                       }
                                                      
                                                        
                                                        #write-host $användare
                                                        Remove-Variable NewPassword
                                                    }
                                                    
                                                    Export-UsersAndPasswords
                                                }

                                          'No' { $popUpMessage3 =  [System.Windows.MessageBox]::Show("Du valde No, så inget lösenord sattes")  }

                                                                }
                       }
                           else
                           {
                                [System.Windows.MessageBox]::Show("Något gick fel så det blev fler än 40 användare som skulle återställas","För många elever","Ok","Warning")
                           }
                        
                    }
    # Exportera användare och lösenord
         function Export-UsersAndPasswords 
                    
                 {
                        $usersToExport = @()
                        Add-Type -AssemblyName System.Windows.Forms
                        $saveFileDialog=New-Object System.Windows.Forms.SaveFileDialog
                        $saveFileDialog.Title = "Spara CSV med användarnamn och lösenord"
                        $saveFileDialog.Filter = "Csv-fil | *.csv"


                            
                                
                                foreach ($item in $usersAndPasswords)
                                    {
                                      if (-not $usersWithoutAccess.Contains($item.Användarnamn))
                                      {
                                      $usersToExport += $item
                                      }
                                    }

                                        if ([bool]$usersToExport)
                                        {
                                            if($SaveFileDialog.ShowDialog() -eq 'Ok'){
                                            $usersToExport | export-csv -Path $saveFileDialog.FileName -Encoding UTF8 -Delimiter ";" -NoTypeInformation
                                            Invoke-Item -Path $saveFileDialog.FileName
                                                                                    }
                                        }

                                
                            
                            Remove-Variable usersAndPasswords -Scope Global 

                            
                 }   

              
           
           #Events
           
            $comboboxSkola.add_MouseLeave({ 
                    get-klassfyllcombo
                                    })

            $buttonsökanvändare.add_click({ 
                    get-elever
                                    })

            $buttonSättaLösenord.add_click({
                    List-UsersAndPassword
                    set-userpassword 
                      
                                    })


       
$Form.ShowDialog() | out-null

                    

                